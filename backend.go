package backend

func ReverseEcho(s string) string {
	rs := ""
	for i := len(s) - 1; i >= 0; i-- {
		rs += string(s[i])
	}

	return rs
}
