package backend_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/avojan/echo"
	"testing"
)

func TestReverseEcho(t *testing.T) {
	rs := backend.ReverseEcho("hello")
	assert.Equal(t, "olleh", rs)
}
