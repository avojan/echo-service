package main

import (
	"fmt"
	"gitlab.com/avojan/echo"
	"io/ioutil"
	"net/http"
)

func EchoHandler(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	b, err := ioutil.ReadAll(r.Body)

	ret := ""
	if err != nil {
		ret = "Error when reading a request body"
	} else {
		ret = string(b)
		fmt.Println("Got " + ret)
	}

	rs := backend.ReverseEcho(ret)

	w.Write([]byte(rs))
}
