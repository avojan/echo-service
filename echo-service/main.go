package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"os"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8000"
	}
	fmt.Println("Listening on port " + port)
	router := mux.NewRouter()
	router.HandleFunc("/echo", EchoHandler).Methods("POST")
	log.Fatal(http.ListenAndServe(":"+port, router))
}
